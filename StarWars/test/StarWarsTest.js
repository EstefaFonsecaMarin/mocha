const expect = require('chai').expect;
const fetch  = require('node-fetch');
 
describe('Fetch', function () {
  it('Status del api Starwars', async () => {
    var response = await fetch('https://swapi.co/api/starships/?format=json');
    expect(response.status).to.be.equal(200);
    var user = await response.json();
    expect(user).to.be.an('Object');
  });
});